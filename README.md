# SAP-1 Computer using Verilog HDL

This repo is for maintaining ONLY the source codes and collaborating.

User manual:

 1. *Clone* the repo in your computer wherever you want to.
 2. Let's say the directory is "C://CSE413/Project/source/"
 3. Launch **Modelsim** and create new project: **File** > **New** > **Project**
 4. Give the *project name* "SAP-1" and  *project location* according to your choice.
 5. Now, you can **Create New File** but in the *File name* section browse to the directory from *step 2*.
 
So, we are keeping the source files and project files in separate folders to ensure flawless collaboration.
*If you want to make some stimulus module for testing your module then avoid committing that stimulus module*

*To use direct command to avoid all those chunky UI stuffs, first compile your file and then run this-*
```bash
vsim work.stimulus; add wave sim:/stimulus/*; run -all
```

*Command line instructions for dealing with Git-*

To **Commit**:
```bash
git add --all
git commit -m "your commit message"
```

To **Sync**:
```bash
git pull --rebase
git push
```