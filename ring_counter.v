
module RINGCOUNTER(states,clk,clear);
  input clear, clk;
  output [6:1] states;
  
  JK f1(w4,states[1],w2,states[6],clk,clear);
  JK f2(states[2],w6,states[1],w4,clk,clear);
  JK f3(states[3],w8,states[2],w6,clk,clear);
  JK f4(states[4],w10,states[3],w8,clk,clear);
  JK f5(states[5],w12,states[4],w10,clk,clear);
  JK f6(states[6],w2,states[5],w12,clk,clear);
endmodule
/*
module stimulus;
  reg clk, clear;
  wire [5:0] states;
  
  RINGCOUNTER counter(states,clk,clear);
  
  initial
  clk = 1'b0;
  
  initial
  forever #5 clk = ~clk;
  
  initial
  begin
  clear = 1'b0;
  #5 clear = 1'b1;
  #70 $stop;
  end
endmodule
*/