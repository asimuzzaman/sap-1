
//negedge triggered clear and clock
module JK(q,qbar,j,k,clk,clear);
	input j,k,clk,clear;
	output q,qbar;
	
	wire w1,w2,w3,w3n,w4,w5,clear;
	not(clkn,clk);
	nand n1(w1,qbar,j,clk,clear);
	//nand n1(w1,qbar,j,clkn,clear);
	nand n2(w2,clk,k,q);
	//nand n2(w2,clkn,k,q);
	nand n3(w3,w1,w3n);
	nand n4(w3n,w3,clear,w2);
	nand n5(w4,w3,clkn);
	//nand n5(w4,w3,clk);
	nand n6(w5,clkn,w3n);
	//nand n6(w5,clk,w3n);
	nand n7(q,w4,qbar);
	nand n8(qbar,q,clear,w5);
endmodule
/*
module stimulus;
  reg J,K,CLK,CLEAR;
  wire Q, QBAR;
  
  JK f1(Q,QBAR,J,K,CLK,CLEAR);
  
  initial
  CLK = 1'b0;
  
  initial
  forever #5 CLK = ~CLK;
  
  initial
  begin
    CLEAR = 1'b0;
    #6 CLEAR = 1'b1;
    #3 J = 1'b1; K = 1'b0;
    #6 J = 1'b0; K = 1'b0;
    #7 J = 1'b0; K = 1'b1;
    #4 J = 1'b1; K = 1'b0;
    #9 J = 1'b1; K = 1'b1;
    #20 $stop;
  end
endmodule
*/