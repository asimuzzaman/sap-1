
module SAP1;
  wire CP,EP,LMBAR,CEBAR,LIBAR,EIBAR,LABAR,EA,SU,EU,LBBAR,LOBAR;
  wire [7:0] WBUS, actas, reg_b;
  wire [3:0] RAM_ADDR, INSTRUCTION;
  wire HLT; //Implementation of HLT
  reg clk, clear;
  
  wire clearbar;
  not(clearbar,clear);
  wire clkbar;
  not(clkbar,clk);
  
  CONTROLLER c1(CP,EP,LMBAR,CEBAR,LIBAR,EIBAR,LABAR,EA,SU,EU,LBBAR,LOBAR,INSTRUCTION,clearbar,clk,HLT);
  
  PC pc(WBUS[3:0],clk,clearbar,CP,EP);
  MAR mar(RAM_ADDR,WBUS[3:0],LMBAR,clk);
  RAM ram(WBUS,RAM_ADDR,CEBAR);
  IR ir(INSTRUCTION,WBUS[3:0],WBUS,LIBAR,clk,clear,EIBAR);
  AC ac(WBUS, actas, WBUS, LABAR, clk, EA);
  ADD_SUB alu(WBUS, reg_b, actas, SU, EU);
  B_REG b_reg(reg_b,WBUS,clk,LBBAR);
  OUTPUT out(WBUS,clk,LOBAR);
  
  initial
  clk = 1'b0;
  
  /*initial
  forever #5 clk = ~clk;*/
  always
  begin
      if(HLT == 1'b0)
        begin
          $display("HLT encountered");
          #5 clk = 1'b0;
          $stop;
        end
      else
        #5 clk = ~clk;
  end
  
  //monitoring WBUS
  always @(WBUS)
   $display("WBUS = %b", WBUS);
  
  initial
  begin
    clear = 1'b1;
    #5 clear = 1'b0;
    //#400 $stop;
  end
  
endmodule