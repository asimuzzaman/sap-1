
module B_REG(toaddsub,bus_in,clk,lbbar);
  input clk, lbbar;
  input [7:0] bus_in;
  output [7:0] toaddsub;

  D_FF d0(toaddsub[0], ,bus_in[0],clk, 1'b1,1'b1,lbbar);
  D_FF d1(toaddsub[1], ,bus_in[1],clk, 1'b1,1'b1,lbbar);
  D_FF d2(toaddsub[2], ,bus_in[2],clk, 1'b1,1'b1,lbbar);
  D_FF d3(toaddsub[3], ,bus_in[3],clk, 1'b1,1'b1,lbbar);
  D_FF d4(toaddsub[4], ,bus_in[4],clk, 1'b1,1'b1,lbbar);
  D_FF d5(toaddsub[5], ,bus_in[5],clk, 1'b1,1'b1,lbbar);
  D_FF d6(toaddsub[6], ,bus_in[6],clk, 1'b1,1'b1,lbbar);
  D_FF d7(toaddsub[7], ,bus_in[7],clk, 1'b1,1'b1,lbbar);
  
  always @(toaddsub)
    $display("From %m at %d, Content of B = %b",$time, toaddsub);
endmodule