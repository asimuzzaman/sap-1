//bus_out = BUS output (8 bit), addr = Address from MAR (4 bit), CE = Enable RAM to BUS (active low)
module RAM(bus_out,addr,ce);
  input [3:0] addr;
  input ce;
  output reg [7:0] bus_out;
  reg [7:0] memory [0:15];
  integer i;
  
  initial
  begin
    $readmemb("prog1.bin", memory);
    
    $display("RAM CONTENT");
    for(i=0; i < 16; i = i + 1)
      $display("Memory [%0d] = %b", i, memory[i]);
  end
  
  always @(ce or addr)
  begin
    if(ce) //RAM output is disabled
      bus_out = 8'bz;
    else
      begin
        case (addr)
          4'b0000: bus_out = memory[0];
          4'b0001: bus_out = memory[1];
          4'b0010: bus_out = memory[2];
          4'b0011: bus_out = memory[3];
          4'b0100: bus_out = memory[4];
          4'b0101: bus_out = memory[5];
          4'b0110: bus_out = memory[6];
          4'b0111: bus_out = memory[7];
          4'b1000: bus_out = memory[8];
          4'b1001: bus_out = memory[9];
          4'b1010: bus_out = memory[10];
          4'b1011: bus_out = memory[11];
          4'b1100: bus_out = memory[12];
          4'b1101: bus_out = memory[13];
          4'b1110: bus_out = memory[14];
          4'b1111: bus_out = memory[15];
        endcase
      end
  end
  
  always @(bus_out)
    $display("From %m at %d, Memory content = %b for Address = %b",$time,bus_out,addr);
  
endmodule
/*
module stimulus;
  reg CE;
  reg [3:0] ADDR;
  wire [7:0] BUS_OUT;
  
  RAM r1(BUS_OUT,ADDR,CE);
  
  initial
  begin
    CE = 1'b1;
    #10 CE = 1'b0; ADDR = 4'b0011;
    #10 ADDR = 4'b1111;
    #20 $stop;
  end
endmodule
*/