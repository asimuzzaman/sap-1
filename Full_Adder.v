//`timescale 1ns / 1ps

module ha (s, c, a, b); //create two ha for fa and connect the wiring
    output s, c;
    input a, b;
    
    and a1(c, a, b);
    xor x1(s, a, b);
endmodule

module fa (s, cout, a, b, cin);
    output s, cout;
    wire d, e, f; 
    input a, b, cin;
    
    ha h1(d, f, a, b);
    ha h2(s, e, d, cin);
    or o1(cout, e, f);
    
endmodule  

/*module stimulus;
  
    wire S, Cout;
    reg A, B, Cin;
    
    fa f(S, Cout, A, B, Cin);
    
    initial 
    begin
      $monitor($time, "A = %b, B = %b, Cin= %b, Sum = %b, Cout = %b" , A, B, Cin, S, Cout);
    end
    
    initial
      begin
          A = 1'b0; B = 1'b0; Cin = 1'b0;
          #2 A = 1'b0; B = 1'b0; Cin = 1'b1;
          #2 A = 1'b0; B = 1'b1; Cin = 1'b0;
          #2 A = 1'b0; B = 1'b1; Cin = 1'b1;
          #2 A = 1'b1; B = 1'b0; Cin = 1'b0;
          #2 A = 1'b1; B = 1'b0; Cin = 1'b1;
          #2 A = 1'b1; B = 1'b1; Cin = 1'b0;
          #2 A = 1'b1; B = 1'b1; Cin = 1'b1;          
          #2 $finish;
      end
endmodule*/