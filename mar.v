module MAR(ram_addr,bus_in,lm,clk);
  input lm, clk;
  input [3:0] bus_in;
  output [3:0] ram_addr;
  
  D_FF f0(.q(ram_addr[0]),.d_in(bus_in[0]),.clk(clk),.enable(lm),.clear(1'b1),.preset(1'b1));
  D_FF f1(.q(ram_addr[1]),.d_in(bus_in[1]),.clk(clk),.enable(lm),.clear(1'b1),.preset(1'b1));
  D_FF f2(.q(ram_addr[2]),.d_in(bus_in[2]),.clk(clk),.enable(lm),.clear(1'b1),.preset(1'b1));
  D_FF f3(.q(ram_addr[3]),.d_in(bus_in[3]),.clk(clk),.enable(lm),.clear(1'b1),.preset(1'b1));
  
  always @(ram_addr)
    $display("From %m at %d, RAM address = %b",$time, ram_addr);
endmodule
/*
module testbench;
  //reg D, CLK, CLEAR, PRESET, ENABLE;
  reg [3:0] BUS_IN;
  reg CLK, LM;
  //wire Q, QBAR;
  wire [3:0] RAM_ADDR;
  
  //D_FF f1(Q,QBAR,D,CLK,CLEAR,PRESET,ENABLE);
  MAR m1(RAM_ADDR,BUS_IN,LM,CLK);
  
  initial
  CLK = 1'b0;
  
  initial
  forever
  #5 CLK = ~CLK;
  
  initial
  begin
    LM = 1'b0;
    #7 BUS_IN = 4'b0101;
    #9 BUS_IN = 4'b1010;
    #11 LM = 1'b1;
    #4 BUS_IN = 4'b0111;
    #20 $stop;
  end
endmodule
*/